<form method="POST" action="{{ url('/post/' . $post->id) }}">
    @method("PUT")
    @csrf
    <input type="text" name="title" value="{{ $post->title }}">
    <textarea type="text" name="body">{{ $post->body }}</textarea>
    <button type="submit">save</button>
</form>

<a href="{{ url('/post/'.$post->id) }}">show post</a>
