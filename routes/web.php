<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::group(['middleware' => ['can:hasPermission,App\Post']], function (){
//    Route::resource('post', 'PostController');
//});

Route::get('post', 'PostController@index');
Route::get('post/create', 'PostController@create')->middleware('can:create,App\Post');
Route::get('post/{id}/edit', 'PostController@edit')->middleware('can:edit,App\Post');
Route::get('post/{id}', 'PostController@show')->middleware('can:show,App\Post');
Route::post('post', 'PostController@store')->middleware('can:store,App\Post');
Route::put('post/{id}', 'PostController@update')->middleware('can:update,App\Post');
Route::delete('post/{id}', 'PostController@destroy')->middleware('can:destroy,App\Post');
