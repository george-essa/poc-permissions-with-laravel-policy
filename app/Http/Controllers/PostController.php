<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = auth()->user()->posts;
        $this->authorize('index', Post::class);
        return view('post.index', compact("posts"));
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('post.edit', compact("post"));
    }

    public function update($id)
    {
        $posts = Post::find($id);
        $posts->update(\request()->all());
        return redirect("post");
    }

    public function create()
    {
        return view('post.create');
    }

    public function show($id)
    {
        $post = Post::find($id);
        return view('post.show', compact("post"));
    }

    public function store()
    {
        Post::create([
            "title" => \request("title"),
            "body" => \request("body"),
            "user_id" => auth()->user()->id,
        ]);
        return redirect("post");
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $this->authorize('delete', $post);
        $post->delete();
        return redirect("post");
    }


}
