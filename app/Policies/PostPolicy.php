<?php

namespace App\Policies;

use App\Post;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;
    public function index(User $user)
    {
        $permission = $user->permissions()->where("title", "index post")->count();
        return $permission > 0;
    }

    public function edit(User $user)
    {
        $permission = $user->permissions()->where("title", "edit post")->count();
        return $permission > 0;
    }

    public function update(User $user)
    {
        $permission = $user->permissions()->where("title", "update post")->count();
        return $permission > 0;
    }

    public function create(User $user)
    {
        $permission = $user->permissions()->where("title", "create post")->count();
        return $permission > 0;
    }

    public function store(User $user)
    {
        $permission = $user->permissions()->where("title", "store post")->count();
        return $permission > 0;
    }

    public function delete(User $user)
    {
        $permission = $user->permissions()->where("title", "delete post")->count();
        return $permission > 0;
    }


    public function show(User $user)
    {
        $permission = $user->permissions()->where("title", "show post")->count();
        return $permission > 0;
    }

}

